import { Injectable } from '@angular/core';

import { Item } from '../../models/item';

@Injectable()
export class Items {
  items: Item[] = [];

  defaultItem: any = {
    "name": "Burt Bear",
    "profilePic": "assets/img/speakers/bear.jpg",
    "about": "Burt is a Bear.",
  };


  constructor() {
    let items = [
      {
        "name": "Carne",
        "profilePic": "assets/img/speakers/carne-vermelha-450x324.jpg",
        "about": "2,5kg"
      },
      {
        "name": "Arroz",
        "profilePic": "assets/img/speakers/arroz.jpg",
        "about": "4kg"
      },
      {
        "name": "Tomate",
        "profilePic": "assets/img/speakers/tomate.jpg",
        "about": "1,7kg"
      },
      {
        "name": "Leite",
        "profilePic": "assets/img/speakers/leite.jpg",
        "about": "5L"
      },
      {
        "name": "Ovos",
        "profilePic": "assets/img/speakers/ovos.jpg",
        "about": "12 unidades"
      },
    ];

    for (let item of items) {
      this.items.push(new Item(item));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.items;
    }

    return this.items.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item: Item) {
    this.items.push(item);
  }

  delete(item: Item) {
    this.items.splice(this.items.indexOf(item), 1);
  }
}
